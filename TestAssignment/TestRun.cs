﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment1_30105247;

namespace TestAssignment
{
    [TestClass]
    public class TestRun
    {
        [TestMethod]
        public void TestHospital()
        {
            Hospital h = new Hospital();

            String str1 = this.ReadFromFile(Program.OUTPUT_FILE);
            String str2 = this.ReadFromFile(Program.TEST_FILE);

            Assert.IsTrue(str1.Equals(str2));
        }

        public String ReadFromFile(String path)
        {
            string text = System.IO.File.ReadAllText(path);
            return text;
        }
    }
}
