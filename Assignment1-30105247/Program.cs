﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Assignment1_30105247
{
    public class Program
    {
        public const String OUTPUT_FILE = @"c:\temp\MyTest.txt";
        public const String TEST_FILE = @"c:\temp2\MyTest.txt";
        public static void Main(String[] args)
        {
            String str = new Hospital().Run();

            byte[] toBytes = Encoding.ASCII.GetBytes(str);
            writeToFileStream(toBytes);
            Console.In.Read();
        }

        public static void writeToFileStream(Byte[] str)
        {
            var fileTest = File.Open(OUTPUT_FILE, FileMode.Create);
            fileTest.Write(str, 0, str.Length - 1);

            fileTest.Flush();
            fileTest.Close();
        }
    }
}
