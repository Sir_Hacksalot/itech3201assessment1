﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_30105247
{
    public class Hospital
    {
        private ArrayList listOfDoctors;
        private ArrayList listOfPatients;

        public Hospital()
        {
            listOfDoctors = new ArrayList();
            listOfPatients = new ArrayList();
        }

        public String Run()
        {
            AddPatients();
            AddDoctors();

            Console.Out.WriteLine(TitleString.Continue);

            String output = TitleString.Commencing;

            output += TitleString.RegDrs;
            output += getDoctors();

            output += TitleString.PatientsBeforeAssignedDrs;
            output += getPatients();

            output += TitleString.AssigningDrs;
            output += AssignPatientsToDrs();

            output += TitleString.PatientsAfterAssignedDrs;
            output += getPatients();

            output += TitleString.DoctorsAfterPatientsAssigned;
            output += getDoctors();
            
            return output;
        }

        private string AssignPatientsToDrs()
        {
            String output = "";
            for (int i = 0; i < listOfPatients.Count; i++)
            {
                Patient temp = (Patient)listOfPatients[i];
                if (temp.AssignDoctor(listOfDoctors) == false)
                {
                    output += TitleString.CannotAssignPatient + temp.Name + TitleString.NoDrAvail;
                    output += temp.AssignPatient();
                }
                else
                {
                    output += TitleString.AssigningDrTo + temp.Name + "\n";
                }
            }
            return output;
        }

        private string getPatients()
        {
            String output = "";
            for (int i = 0; i < listOfPatients.Count; i++)
            {
                output += listOfPatients[i].ToString() + "\n";
            }
            return output;
        }

        private void AddPatients()
        {
            listOfPatients.Insert(listOfPatients.Count, new PrivatePatient("Fred Bear", 29, "Private", 10, "Ben Casey", "HCF236788"));
            listOfPatients.Insert(listOfPatients.Count, new PublicPatient("Betty Davis", 37, "Public", 7, "Medicare2562"));
            listOfPatients.Insert(listOfPatients.Count, new PrivatePatient("Bella Plant", 25, "Private", 23, "Ben Casey", "HCF265123"));
            listOfPatients.Insert(listOfPatients.Count, new PrivatePatient("Thomas Edison", 12, "Private", 2, "Doogie Howser", "HCF265988"));
            listOfPatients.Insert(listOfPatients.Count, new PublicPatient("Chris Smith", 56, "Public", 1, "Medicare5678"));
            listOfPatients.Insert(listOfPatients.Count, new PublicPatient("Chris Jones", 56, "Public", 1, "Medicare5679"));
            listOfPatients.Insert(listOfPatients.Count, new PublicPatient("Chris Simpson", 56, "Public", 1, "Medicare5690"));
            listOfPatients.Insert(listOfPatients.Count, new PublicPatient("Chris Christoph", 56, "Public", 1, "Medicare5878"));
        }

        private void AddDoctors()
        {
            listOfDoctors.Add(new Doctor("Ben Casey", 32, 3, "Ear, Nose, Throat", 100));
            listOfDoctors.Add(new Doctor("Hawkeye Pierce", 47, 4, "Heart", 100));
            listOfDoctors.Add(new Doctor("Doogie Howser", 22, 2, "Paediatrician", 150));
        }

        private string getDoctors()
        {
            String output = "";
            for (int i = 0; i < listOfDoctors.Count; i++)
            {
                Doctor temp = ((Doctor)listOfDoctors[i]);
                output += temp.ToString() + "\n";

                if (temp.HasPatients())
                {
                    output += temp.PrintListOfPatients();
                }
                else
                {
                    output += TitleString.NoPatientsAssigned;
                }
                output += "\n";
            }
            output += "\n";
            return output;
        }
    }
}
