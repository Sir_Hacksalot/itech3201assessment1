﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_30105247
{
    public abstract class Patient : Person
    {
        protected Doctor assignedDoctor; // Doctor assigned to Patient
        protected static int pIdNo = 100000; // No get and set methods - internal use only
        protected String patientType; // can be "Private" or "Public" - public "default"
        protected int hoursTreated; // No of hours patient treated by Doctor

        public Patient(String name, int age, String patientType, int hoursTreated) : base(name, age)
        {
            assignedDoctor = null;
            this.patientType = patientType;
            this.hoursTreated = hoursTreated;
            SetIdentifier();
        }

        public abstract bool AssignDoctor(ArrayList listOfDoctors);

        public abstract String AssignPatient();

        public override void SetIdentifier()
        {
            this.Identifier = "P" + pIdNo++;
        }

        public override String ToString()
        {
            return this.Name + "\n\t Identifier: " + this.Identifier;       
        }

        protected string GetDoctorFee()
        {
            String fee;
            if (this.assignedDoctor == null)
            {
                fee = "not assigned as yet";
            }
            else
            {
                fee = this.assignedDoctor.Name
                    + "\n\t Fee for consultation = $"
                    + this.hoursTreated * this.assignedDoctor.GetHourlyRate();
            }
            return fee;
        }
    }

}
