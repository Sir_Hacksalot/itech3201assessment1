﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_30105247
{
    class TitleString
    {
        public static readonly String Commencing = "\n\n--- Test Output Commencing ---\n";
        public static readonly String RegDrs = "___________________\n\nList of registered doctors\n___________________\n\n";
        public static readonly String PatientsBeforeAssignedDrs = "\n___________________\n\nList of patients before doctors assigned\n___________________\n\n";
        public static readonly String AssigningDrs = "\n___________________\n\n Assigning Doctors to Patients\n___________________\n";
        public static readonly String CannotAssignPatient = "\n*******************\n Cannot assign patient to ";
        public static readonly String NoDrAvail = "- No available doctors\n*******************\n";
        public static readonly String PatientsAfterAssignedDrs = "\n___________________\n\nList of patients after doctors assigned\n___________________\n";
        public static readonly String NoPatientsAssigned = "No patients assigned to this doctor as yet";
        public static readonly String DoctorsAfterPatientsAssigned = "___________________\n\nList of doctors after patients assigned\n___________________\n" + "\n";
        public static readonly String AssigningDrTo = "Assigning a doctor to ";
        public static readonly String Continue = "\n\n--- Hit Enter key to continue --- \n";

    }
}
