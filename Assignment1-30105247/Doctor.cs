﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_30105247
{
    public class Doctor : Person
    {
        String specialisation;		// Doctor's special skill
        ArrayList assignedPatients;		// Doctor's personal list of patients
        int maxPatients;		// Maximum no. of patients a Dr can have at one time
        double hourlyRate;			// Hourly rate of pay
        static int drIdNo = 1000;	// Internal use only - no get/set methods
   
        public Doctor() : base()
        {
            maxPatients = 0;
            specialisation = "Unknown";
            hourlyRate = 100;
            SetIdentifier();
            assignedPatients = new ArrayList();
        }

        public Doctor(String name, int age, int maxPatients, String specialisation, double hourlyRate) 
            : base(name, age)
        {
            this.maxPatients = maxPatients;
            this.specialisation = specialisation;
            this.hourlyRate = hourlyRate;
            SetIdentifier();
            assignedPatients = new ArrayList();
        }

        public double GetHourlyRate()
        {
            return this.hourlyRate;
        }

        public bool HasPatients()
        {
            return assignedPatients.Count > 0;
        }

        public override void SetIdentifier()
        {
            Identifier = "D" + drIdNo++;
        }

        public override String ToString()
        {
            return "*******************\n Dr " + Name
                + "\n\t id number: " + Identifier 
                + "\n\t Max Number of Patients: " + maxPatients;
        }
 
        public bool AssignPatientToDoctorsList(Patient patient)
        {
            if (assignedPatients.Count < maxPatients)
            {
                assignedPatients.Insert (assignedPatients.Count, patient);
                return true;
            }
            return false;
        }

        public String PrintListOfPatients()
        {
            String temp = "";
            temp += "\nList of Patients for Dr " + Name + "\n\n";
            for (int i = 0; i < assignedPatients.Count; i++)
            {
                temp += assignedPatients[i] + "\n";
            }
            return temp;
        }      
    }

}
