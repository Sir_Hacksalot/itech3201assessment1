﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_30105247
{
    class PrivatePatient : Patient
    {
        private String privateHealthFundNo; 
        private String preferredDoctor;

        public PrivatePatient(String name, int age, String patientType, int hoursTreated, 
            String preferredDoctor, String privateHealthFundNo) 
            : base(name, age, patientType, hoursTreated)
        {
            this.preferredDoctor = preferredDoctor;
            this.privateHealthFundNo = privateHealthFundNo;
        }

        public override string ToString()
        {
            String temp = "Private Patient - " + base.ToString()
                        + "\n\t Preferred Doctor " + preferredDoctor
                        + "\n\t Private Health Fund Number: " + privateHealthFundNo
                        + "\n\t Assigned Doctor: ";

            return temp + GetDoctorFee() + "\n";
        }

        public override String AssignPatient()
        {
            return "Reassigning Private Patient " + Name + " to a different hospital";
        }

        public override bool AssignDoctor(ArrayList listOfDoctors)
        {
            for (int i = 0; i < listOfDoctors.Count; i++)
            {
                Doctor temp = (Doctor)listOfDoctors[i];
                if (temp.Name == this.preferredDoctor)
                {
                    assignedDoctor = temp;
                    return (temp.AssignPatientToDoctorsList(this));
                }
            }
            return false;
        }
    }
}
