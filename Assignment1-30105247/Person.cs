﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_30105247
{
    public abstract class Person
    {
        private String name;
        private int age;
        private String identifier;

        public Person()
        {
            this.name = "Unknown";
            this.age = 0;
            this.identifier = "";
        }

        public Person(String name, int age)
        {
            this.name = name;
            this.age = age;
        }

        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        public String Identifier
        {
            get { return identifier; }
            set { identifier = value; }
        }

        public abstract void SetIdentifier();

        public abstract override string ToString();
    }
}
