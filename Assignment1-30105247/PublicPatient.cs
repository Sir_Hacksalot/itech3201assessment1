﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_30105247
{
    class PublicPatient : Patient
    {
        private String medicareNo;

        public PublicPatient(String name, int age, String patientType, int hoursTreated, String medicareNo) 
            : base(name, age, patientType, hoursTreated)
        {
            this.medicareNo = medicareNo;      
        }

        public override String ToString()
        {
            String temp = "Public Patient - " + base.ToString()
                        + "\n\t Medicare Number: " + medicareNo
                        + "\n\t Assigned Doctor: ";

            return temp + GetDoctorFee() + "\n";
        }

        public override String AssignPatient()
        {
            return "Assigning public patient " + this.Name + " to the waiting list until a doctor becomes available";
        }

        public override bool AssignDoctor(ArrayList listOfDoctors)
        {
            for (int i = 0; i < listOfDoctors.Count; i++)
            {
                Doctor temp = (Doctor)(listOfDoctors[i]);
                if (temp.AssignPatientToDoctorsList(this))
                {
                    assignedDoctor = temp;
                    return true;
                }
            }
            return false;
        }
    }
}
